<?php

namespace Traits;

use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Filesystem;

trait RemoveTrait
{
    /**
     * Deletes a directory.
     *
     * @param string $path      absolute path of directory
     * @return bool             return true if directory is created
     */
    public static function delete(string $path): bool
    {
        try {
            $fs = new Filesystem();
            $fs->remove($path);
            return true;
        } catch (IOException $exception) {
            echo "An error occurred while deleting your directory at " . $exception->getPath();
        }

        return false;
    }
}
