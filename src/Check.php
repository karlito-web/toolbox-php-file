<?php declare(strict_types=1);

namespace KarlitoWeb\Toolbox\File;

use SplFileInfo;
use KarlitoWeb\Toolbox\File\Interfaces\CheckInterface;
use Symfony\Component\Filesystem\Filesystem;

/**
 * @author      Karlito15                               <giancarlo.palumbo@free.fr>
 * @license     https://opensource.org/license/mit/     MIT
 * @link        https://www.php.net/manual/fr/ref.filesystem.php
 * @package     karlito-web/toolbox-php-file
 * @subpackage  symfony/filesystem
 * @subpackage  brandonwamboldt/utilphp
 * @version     3.0.2
 */
class Check implements CheckInterface
{
	/**
	 * Checks the existence of files or directories.
	 *
	 * @param string $path  A filename, an array of files, or a \Traversable instance to check
	 * @return bool         return true if the file exists, false otherwise
	 */
	public static function isExist(string $path): bool
	{
		$fs = new Filesystem();

		return $fs->exists($path);
	}

	/**
	 * Tells if the file is a directory.
	 *
	 * @param string $path
	 * @return bool
	 */
	public static function isDir(string $path): bool
	{
		$spl = new SplFileInfo($path);

		return $spl->isDir();
	}

	/**
	 * Tells if the object references a regular file.
	 *
	 * @param string $path
	 * @return bool
	 */
	public static function isFile(string $path): bool
	{
		$spl = new SplFileInfo($path);

		return $spl->isFile();
	}
}
