<?php

declare(strict_types=1);

namespace KarlitoWeb\Toolbox\File;

use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Filesystem;
use utilphp\util as UtilPHP;

/**
 * @author      Karlito15                               <giancarlo.palumbo@free.fr>
 * @license     https://opensource.org/license/mit/     MIT
 * @link        https://www.php.net/manual/fr/ref.filesystem.php
 * @package     karlito-web/toolbox-php-file
 * @subpackage  symfony/filesystem
 * @subpackage  brandonwamboldt/utilphp
 * @version     3.0.2
 */
class System
{
    /**
     * Renames a file or a directory.
     *
     * @param string $source
     * @param string $destination
     * @param bool   $overwrite
     * @return void
     * @throws IOException When target file or directory already exists
     * @throws IOException When origin cannot be renamed
     */
    public static function rename(string $source, string $destination, bool $overwrite = false): void
    {
        try {
            $fs = new Filesystem();
            $fs->rename($source, $destination, $overwrite);
        } catch (IOException $exception) {
            echo "An error occurred while renaming your file at " . $exception->getPath();
        }
    }

    /**
     * Change the owner of an array of files or directories.
     *
     * @param string|iterable $files     A filename, an array of files, or a \Traversable instance to change owner
     * @param string|int      $user      A user name or number
     * @param bool            $recursive Whether change the owner recursively or not
     * @return void
     * @throws IOException When the change fails
     */
    public static function chown($files, $user, bool $recursive = false): void
    {
        try {
            $fs = new Filesystem();
            $fs->chown($files, $user, $recursive);
        } catch (IOException $exception) {
            // echo "An error occurred while creating your file at " . $exception->getPath();
        }
    }

    /**
     * Change the group of an array of files or directories.
     *
     * @param string|iterable $files     A filename, an array of files, or a \Traversable instance to change group
     * @param string|int      $group     A group name or number
     * @param bool            $recursive Whether change the group recursively or not
     * @return void
     * @throws IOException When the change fails
     */
    public static function chgrp($files, $group, bool $recursive = false): void
    {
        try {
            $fs = new Filesystem();
            $fs->chgrp($files, $group, $recursive);
        } catch (IOException $exception) {
            // echo "An error occurred while creating your file at " . $exception->getPath();
        }
    }

    /**
     * Change mode for an array of files or directories.
     *
     * @param string|iterable $files     A filename, an array of files, or a \Traversable instance to change mode
     * @param int             $mode      The new mode (octal)
     * @param int             $umask     The mode mask (octal)
     * @param bool            $recursive Whether change the mod recursively or not
     * @return void
     * @throws IOException When the change fails
     */
    public static function chmod($files, int $mode, int $umask = 0000, bool $recursive = false): void
    {
        try {
            $fs = new Filesystem();
            $fs->chmod($files, $mode, $umask, $recursive);
        } catch (IOException $exception) {
            // echo "An error occurred while creating your file at " . $exception->getPath();
        }
    }

    /**
     * Returns the file permissions as a nice string
     *
     * @link https://www.php.net/manual/fr/splfileinfo.getperms.php
     * @param string $path
     * @return string
     * @example '-rw-r--r--'
     */
    public static function getPerms(string $path): string
    {
        return UtilPHP::full_permissions($path);
    }
}
