<?php

declare(strict_types=1);

namespace KarlitoWeb\Toolbox\File\Interfaces;

use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Symfony\Component\Filesystem\Exception\IOException;

/**
 * @author      Karlito15                               <giancarlo.palumbo@free.fr>
 * @license     https://opensource.org/license/mit/     MIT
 * @link        https://www.php.net/manual/fr/ref.filesystem.php
 * @package     karlito-web/toolbox-php-file
 * @subpackage  symfony/filesystem
 * @subpackage  brandonwamboldt/utilphp
 * @version     3.0.2
 */
interface FileInterface
{
	/**
	 * Creates a file.
	 *
	 * @param string $filepath  absolute path of file
	 * @return bool             return true if file is created
	 */
	public static function createFile(string $filepath): bool;

	/**
	 * Reads the content of a $file. Throws an exception Nette\IOException on error occurred.
	 *
	 * @param string $filepath
	 * @return string
	 */
	public static function readFile(string $filepath): string;

    /**
     * Copies a file.
     *
     * @param string $source
     * @param string $destination
     * @param bool   $overwrite
     * @return void
     * @throws FileNotFoundException When originFile doesn't exist
     * @throws IOException           When copy fails
     */
    public static function copyFile(string $source, string $destination, bool $overwrite = true): void;

	/**
	 * Delete a file.
	 *
	 * @param string $path  absolute path of file
	 * @return bool             return true if file is deleted
	 */
	public static function delete(string $path): bool;
}
