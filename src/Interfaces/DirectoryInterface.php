<?php

declare(strict_types=1);

namespace KarlitoWeb\Toolbox\File\Interfaces;

/**
 * @author      Karlito15                               <giancarlo.palumbo@free.fr>
 * @license     https://opensource.org/license/mit/     MIT
 * @link        https://www.php.net/manual/fr/ref.filesystem.php
 * @package     karlito-web/toolbox-php-file
 * @subpackage  symfony/filesystem
 * @subpackage  brandonwamboldt/utilphp
 * @version     3.0.2
 */
interface DirectoryInterface
{
	/**
	 * Creates a directory recursively.
	 *
	 * @param string $path      absolute path of directory
	 * @param int    $mode      mode default :: 0775
	 * @return bool             return true if directory is created
	 */
	public static function makeDirectory(string $path, int $mode = 0775): bool;

	/**
	 * Delete a directory.
	 *
	 * @param string $path  absolute path of file
	 * @return bool         return true if file is deleted
	 */
	public static function delete(string $path): bool;
}
