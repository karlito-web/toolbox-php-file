<?php

declare(strict_types=1);

namespace KarlitoWeb\Toolbox\File;

use KarlitoWeb\Toolbox\File\Interfaces\FileInterface;
use SplFileInfo;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Filesystem;
use Traits\RemoveTrait;

/**
 * @author      Karlito15                               <giancarlo.palumbo@free.fr>
 * @license     https://opensource.org/license/mit/     MIT
 * @link        https://www.php.net/manual/fr/ref.filesystem.php
 * @package     karlito-web/toolbox-php-file
 * @subpackage  symfony/filesystem
 * @subpackage  brandonwamboldt/utilphp
 * @version     3.0.2
 */
class File implements FileInterface
{
    use RemoveTrait;

    /**
     * Creates a file.
     *
     * @param string        $filepath   Absolute path of file
     * @param string|null   $content    The data to write into the file
     * @return bool                     Return true if file is created
     */
    public static function createFile(string $filepath, ?string $content = null): bool
    {
        // Extract Directory & Create it
        if (Check::isExist($filepath) === false) {
            $spl = new SplFileInfo($filepath);
            Directory::makeDirectory($spl->getFileInfo()->getPath());
        }

        try {
            $fs = new Filesystem();
            (is_null($content)) ? $fs->touch($filepath) : $fs->dumpFile($filepath, $content);
            return true;
        } catch (IOException $exception) {
            echo "An error occurred while creating your file at " . $exception->getPath();
        }

        return false;
    }

    /**
     * Appends content to an existing file.
     *
     * @param string        $filepath
     * @param string        $content    The data to write into the file
     * @throws IOException              If the file not writable
     */
    public static function appendToFile(string $filepath, string $content): void
    {
        try {
            $fs = new Filesystem();
            $fs->appendToFile($filepath, $content);
        } catch (IOException $exception) {
            echo "An error occurred while creating your file at " . $exception->getPath();
        }
    }

    /**
     * Reads the content of a file.
     * Throws an exception Nette\IOException on error occurred.
     *
     * @param string $filepath
     * @return string
     */
    public static function readFile(string $filepath): string
    {
        try {
            return \Nette\Utils\FileSystem::read($filepath);
        } catch (\Nette\IOException $exception) {
            echo $exception->getMessage();
        }

        return '';
    }

    /**
     * Copies a file.
     *
     * If the target file is older than the origin file, it's always overwritten.
     * If the target file is newer, it is overwritten only when the
     * $overwriteNewerFiles option is set to true.
     * @param string $source
     * @param string $destination
     * @param bool   $overwrite
     * @return void
     * @throws FileNotFoundException When originFile doesn't exist
     * @throws IOException           When copy fails
     */
    public static function copyFile(string $source, string $destination, bool $overwrite = true): void
    {
        try {
            $fs = new Filesystem();
            $fs->copy($source, $destination, $overwrite);
        } catch (IOException $exception) {
            echo "An error occurred while copying your file at " . $exception->getPath();
        }
    }
}
