<?php declare(strict_types=1);

namespace KarlitoWeb\Toolbox\File;

use KarlitoWeb\Toolbox\File\Interfaces\DirectoryInterface;
use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Filesystem;
use Traits\RemoveTrait;

/**
 * @author      Karlito15                               <giancarlo.palumbo@free.fr>
 * @license     https://opensource.org/license/mit/     MIT
 * @link        https://www.php.net/manual/fr/ref.filesystem.php
 * @package     karlito-web/toolbox-php-file
 * @subpackage  symfony/filesystem
 * @subpackage  brandonwamboldt/utilphp
 * @version     3.0.2
 */
class Directory implements DirectoryInterface
{
    use RemoveTrait;

    /**
     * Creates a directory recursively.
     *
     * @param string $path      absolute path of directory
     * @param int    $mode      mode default :: 0775
     * @return bool             return true if directory is created
     */
    public static function makeDirectory(string $path, int $mode = 0775): bool
    {
        try {
            $fs = new Filesystem();
            $fs->mkdir($path, $mode);
            return true;
        } catch (IOException $exception) {
            echo "An error occurred while creating your directory at " . $exception->getPath();
        }

        return false;
    }

    /**
     * Copy a directory recursively to the target.
     *
     * @param string $source
     * @param string $destination
     * @return bool
     */
    public static function copyDirectory(string $source, string $destination): bool
    {
        try {
            $fs = new Filesystem();
            $fs->mirror($source, $destination);
            return true;
        } catch (IOException $exception) {
            echo "An error occurred while copy your directory at " . $exception->getPath();
        }

        return false;
    }
}
