<?php

namespace KarlitoWeb\Toolbox\File\tests;

use KarlitoWeb\Toolbox\File\Directory;
use PHPUnit\Framework\TestCase;

class DirectoryTest extends TestCase
{
    /** @covers Directory */
    public function testTrue(): void
    {
        $path = __DIR__ . DIRECTORY_SEPARATOR . 'level-1' . DIRECTORY_SEPARATOR . 'level-2';
        $test = Directory::makeDirectory($path);
        $this->assertTrue($test);

        $path = __DIR__ . DIRECTORY_SEPARATOR . 'level-1';
        $test = Directory::deleteDirectory($path);
        $this->assertTrue($test);
    }
}
