<?php

namespace KarlitoWeb\Toolbox\File\tests;

use KarlitoWeb\Toolbox\File\Directory;
use KarlitoWeb\Toolbox\File\File;
use PHPUnit\Framework\TestCase;

class FileTest extends TestCase
{
    /** @covers File */
    public function testTrue(): void
    {
        $path = __DIR__ . DIRECTORY_SEPARATOR . 'level-1' . DIRECTORY_SEPARATOR . 'level-2' . DIRECTORY_SEPARATOR . 'text.txt';
        $test = File::createFile($path);
        $this->assertTrue($test);

        $test = File::deleteFile($path);
        $this->assertTrue($test);

        Directory::deleteDirectory(__DIR__ . DIRECTORY_SEPARATOR . 'level-1');
    }
}
