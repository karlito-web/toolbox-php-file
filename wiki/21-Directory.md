# Usage
#### by Karlito Web

***

``` php
use KarlitoWeb\Toolbox\File\Directory;
```

Creates a directory recursively.
```php
Directory::makeDirectory(string $path, int $mode = 0775): bool
```

Deletes a directory.
```php
Directory::deleteDirectory(string $path): bool
```
