# Usage
#### by Karlito Web

***

``` php
use KarlitoWeb\Toolbox\File\Check;
```

Checks the existence of files or directories.
``` php
Check::isExist(string $path): bool
```

Tells if the file is a directory.
```php
Check::isDir(string $path): bool
```

Tells if the object references a regular file.
```php
Check::isFile(string $path): bool
```
