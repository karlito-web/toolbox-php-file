# Usage
#### by Karlito Web

***

``` php
use KarlitoWeb\Toolbox\File\File;
```

Creates a file.
```php
File::createFile(string $filepath, $content = null): bool
```

Appends content to an existing file.
```php
File::appendToFile(string $filepath, $content): void
```

Reads the content of a file.
```php
File::readFile(string $filepath): string
```

Deletes a file.
```php
File::deleteFile(string $filepath): bool
```
