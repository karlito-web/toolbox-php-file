# Usage
#### by Karlito Web

***

``` php
use KarlitoWeb\Toolbox\File\System;
```

Renames a file or a directory.
```php
System::rename(string $source, string $destination, bool $overwrite = false)
```

Copies a file.
```php
System::copy(string $source, string $destination, bool $overwrite = true)
```

Change the owner of an array of files or directories.
```php
System::chown($files, $user, bool $recursive = false)
```

Change the group of an array of files or directories.
```php
System::chgrp($files, $group, bool $recursive = false)
```

Change mode for an array of files or directories.
```php
System::chmod($files, int $mode, int $umask = 0000, bool $recursive = false)
```

Returns the file permissions as a nice string
```php
System::getPerms(string $path): string
```
